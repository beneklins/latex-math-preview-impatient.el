;;; latex-math-preview-impatient.el --- Instant preview of LaTeX equations via latex-math-preview -*- lexical-binding: t; -*-
;;
;; Copyright (c) 2022 Igor Benek-Lins
;; Based on the works licensed under the GPLv3 licence:
;; * org-latex-impatitent
;; Copyright (c) 2020 Sheng Yang
;; * latex-math-preview
;; Copyright 2009–2021 Takayuki YAMAGUCHI
;; Copyright 2006, 2007, 2008, 2009 Kevin Ryde
;;
;; Author: Igor Benek-Lins <physics@ibeneklins.com>
;; Created: 2020-06-03
;; Modified: 2022-07-13
;; Version: 0.0.1
;; Keywords: tex,tools
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  This package provides instant preview of LaTeX snippets via latex-math-preview.
;;
;;; Licence:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;; Code:
;;;

(require 'image-mode)
(require 's)
(require 'dash)
(require 'org)
(require 'posframe)
(require 'org-element)

(defcustom latex-math-preview-impatient-delay 1
  "Number of seconds to wait before a re-compilation."
  :group 'latex-math-preview-impatient
  :type '(number))

(defcustom latex-math-preview-impatient-scale 1.0
  "Scale of preview."
  :group 'latex-math-preview-impatient
  :type '(float))

(defcustom latex-math-preview-impatient-border-color "black"
  "Color of preview border."
  :group 'latex-math-preview-impatient
  :type '(color))

(defcustom latex-math-preview-impatient-border-width 1
  "Width of preview border."
  :group 'latex-math-preview-impatient
  :type '(integer))

(defcustom latex-math-preview-impatient-user-latex-definitions
  '("\\newcommand{\\ensuremath}[1]{#1}"
    "\\renewcommand{\\usepackage}[2][]{}")
  "Custom LaTeX definitions used in preview.

\\usepackage redefined since MathJax does not support it"
  :group 'latex-math-preview-impatient
  :type '(repeat string))

(defcustom latex-math-preview-impatient-posframe-position-handler
  #'latex-math-preview-impatient-poshandler
  "The handler for posframe position."
  :group 'latex-math-preview-impatient
  :type '(function))

(defcustom latex-math-preview-impatient-posframe-position
  'tex-end
  "The position that will be used by posframe handler."
  :group 'latex-math-preview-impatient
  :type '(choice (const :tag "Current Point" point)
                 (const :tag "End of TeX String" tex-end)))

(defvar latex-math-preview-impatient-keymap
  (let ((map (make-sparse-keymap)))
    (define-key map "\C-g" #'latex-math-preview-impatient-abort-preview)
    map)
  "Keymap for reading input.")

(defvar latex-math-preview-impatient--timer nil)
(defvar-local latex-math-preview-impatient--last-tex-string nil)
(defvar-local latex-math-preview-impatient--last-position nil)
(defvar-local latex-math-preview-impatient--position nil)
(defvar-local latex-math-preview-impatient--last-preview nil)
(defvar-local latex-math-preview-impatient--is-inline nil)

(defun latex-math-preview-impatient-poshandler (info)
  "Default position handler for posframe.

Uses the end point of the current LaTeX fragment for inline math,
and centering right below the end point otherwise. Positions are
calculated from INFO."
  (if latex-math-preview-impatient--is-inline
      (posframe-poshandler-point-bottom-left-corner info)
    (if (fboundp 'posframe-poshandler-point-window-center)
        (posframe-poshandler-point-window-center info)
      (latex-math-preview-impatient--poshandler-point-window-center info))))

(defun latex-math-preview-impatient--poshandler-point-window-center (info)
  "Posframe's position handler.

Get a position which let posframe stay right below current
position, centered in the current window. The structure of INFO
can be found in docstring of `posframe-show'.

This function will be removed once a similar poshandler is
available in upstream."
  (let* ((window-left (plist-get info :parent-window-left))
         (window-width (plist-get info :parent-window-width))
         (posframe-width (plist-get info :posframe-width))
         (posframe-height (plist-get info :posframe-height))
         (y-pixel-offset (plist-get info :y-pixel-offset))
         (ymax (plist-get info :parent-frame-height))
         (window (plist-get info :parent-window))
         (position-info (plist-get info :position-info))
         (header-line-height (plist-get info :header-line-height))
         (tab-line-height (plist-get info :tab-line-height))
         (y-top (+ (cadr (window-pixel-edges window))
                   tab-line-height
                   header-line-height
                   (- (or (cdr (posn-x-y position-info)) 0)
                      ;; Fix the conflict with flycheck
                      ;; https://lists.gnu.org/archive/html/emacs-devel/2018-01/msg00537.html
                      (or (cdr (posn-object-x-y position-info)) 0))
                   y-pixel-offset))
         (font-height (plist-get info :font-height))
         (y-bottom (+ y-top font-height)))
    (cons (+ window-left (/ (- window-width posframe-width) 2))
          (max 0 (if (> (+ y-bottom (or posframe-height 0)) ymax)
                     (- y-top (or posframe-height 0))
                   y-bottom)))))

:autoload
(defun latex-math-preview-impatient-stop ()
  "Stop instant preview of LaTeX snippets."
  (interactive)
  ;; only needed for manual start/stop
  (remove-hook 'after-change-functions #'latex-math-preview-impatient--prepare-timer t)
  ;; (latex-math-preview-impatient--hide)
  ;; (latex-math-preview-impatient--interrupt-rendering)
  (latex-math-preview-delete-buffer)
  (latex-math-preview-quit-window)
  )

(defun latex-math-preview-impatient--prepare-timer (&rest _)
  "Prepare timer to call re-compilation."
  (when latex-math-preview-impatient--timer
    (cancel-timer latex-math-preview-impatient--timer)
    (setq latex-math-preview-impatient--timer nil))
  (if (and (or (derived-mode-p 'org-mode)
               (derived-mode-p 'latex-mode)
               (derived-mode-p 'markdown-mode))
           (latex-math-preview-impatient--in-latex-p))
      (setq latex-math-preview-impatient--timer
            (run-with-idle-timer latex-math-preview-impatient-delay nil #'latex-math-preview-impatient-start))
    ;; (latex-math-preview-impatient--hide)
	(latex-math-preview-delete-buffer)
	(latex-math-preview-quit-window)
	)
  )

(defun latex-math-preview-impatient--add-color (ss)
  "Wrap SS with color from default face."
  (let ((color (face-foreground 'default)))
    (format "\\color{%s}{%s}" color ss)))

(defun latex-math-preview-impatient--equal-or-member (elm target)
  (if (listp target)
      (member elm target)
    (equal elm target)))

(defun latex-math-preview-impatient--in-latex-p ()
  "Return t if current point is in a LaTeX fragment, nil otherwise."
  (cond ((derived-mode-p 'org-mode)
         (let ((datum (org-element-context)))
           (or (memq (org-element-type datum) '(latex-environment latex-fragment))
               (and (memq (org-element-type datum) '(export-block))
                    (equal (org-element-property :type datum) "LATEX")))))
        ((derived-mode-p 'latex-mode)
         (latex-math-preview-impatient--tex-in-latex-p))
        ((derived-mode-p 'markdown-mode)
         (latex-math-preview-impatient--equal-or-member 'markdown-math-face (get-text-property (point) 'face)))
        (t (message "We only support org-mode, latex-mode, and markdown-mode")
           nil)
		)
  )

(defun latex-math-preview-impatient--tex-in-latex-p ()
  "Return t if in LaTeX fragment in `latex-mode', nil otherwise."
  ;; Use AUCTeX's texmathp.
  (texmathp)
  )

(defun latex-math-preview-impatient--get-tex-string ()
  "Return the string of LaTeX fragment."
  (cond ((derived-mode-p 'org-mode)
         (let ((datum (org-element-context)))
           (org-element-property :value datum)))
        ((derived-mode-p 'latex-mode)
         (let (begin end)
           (save-excursion
             (while (latex-math-preview-impatient--tex-in-latex-p)
               (backward-char))
             (setq begin (1+ (point))))
           (save-excursion
             (while (latex-math-preview-impatient--tex-in-latex-p)
               (forward-char))
             (setq end (point)))
           ;; (let ((ss (buffer-substring-no-properties begin end)))
           ;;   (message "ss is %S" ss)
           ;;   ss)
		   )
		 )
        (t "")))

(defun latex-math-preview-impatient--get-tex-position ()
  "Return the end position of LaTeX fragment."
  (cond ((eq latex-math-preview-impatient-posframe-position 'tex-end)
         (cond ((derived-mode-p 'org-mode)
                (let ((datum (org-element-context)))
                  (org-element-property :end datum)))
               ((derived-mode-p 'latex-mode)
                (save-excursion
                  (while (latex-math-preview-impatient--tex-in-latex-p)
                    (forward-char))
                  (point)))
               ((derived-mode-p 'markdown-mode)
                (save-excursion
                  (prop-match-end
                   (text-property--find-end-forward
                    (point) 'face 'markdown-math-face #'yang/equal-or-member))))
               (t (message "Only org-mode, latex-mode, and markdown-mode supported") nil)))
        ((eq latex-math-preview-impatient-posframe-position 'point)
         (point))
        (t (message "latex-math-preview-impatient-posframe-position set incorrectly"))))

:autoload
(defun latex-math-preview-impatient-start (&rest _)
  "Start instant preview."
  (interactive)
  ;; Only used for manual start
  (if (and (or (derived-mode-p 'org-mode)
               (derived-mode-p 'latex-mode)
               (derived-mode-p 'markdown-mode))
       (latex-math-preview-impatient--in-latex-p)
	   )
      (let ((--dummy-- (setq latex-math-preview-impatient--is-inline nil))
            (tex-string (latex-math-preview-impatient--get-tex-string))
			)

          (setq latex-math-preview-impatient--position (latex-math-preview-impatient--get-tex-position)
				)
          (if (and latex-math-preview-impatient--last-tex-string
                   (equal tex-string
                          latex-math-preview-impatient--last-tex-string))
              ;; TeX string is the same, we only need to update posframe
              ;; position.
              (when (and latex-math-preview-impatient--last-position
                         (equal latex-math-preview-impatient--position latex-math-preview-impatient--last-position)
						 )
                ;; (latex-math-preview-impatient--show)
				)
            ;; A new rendering is needed.
			(latex-math-preview-expression)
			)
		  ;; )
		)
	)
  )

(defun latex-math-preview-impatient-abort-preview ()
  "Abort preview."
  (interactive)
  ;; (latex-math-preview-impatient--interrupt-rendering)
  (define-key latex-math-preview-impatient-keymap (kbd "C-g") nil)
  ;; (setq latex-math-preview-impatient--force-hidden t)
  ;; (latex-math-preview-impatient--hide)
  (latex-math-preview-delete-buffer)
  (latex-math-preview-quit-window)
  )

:autoload
(define-minor-mode latex-math-preview-impatient-mode
  "Instant preview of LaTeX in org-mode"
  nil nil latex-math-preview-impatient-keymap
  (if latex-math-preview-impatient-mode
      (progn
        ;; (setq latex-math-preview-impatient--output-buffer
        ;;       (concat latex-math-preview-impatient--output-buffer-prefix (buffer-name)))
        (add-hook 'post-command-hook #'latex-math-preview-impatient--prepare-timer nil t))
    (remove-hook 'post-command-hook #'latex-math-preview-impatient--prepare-timer t)
    (latex-math-preview-impatient-stop)))

;; Advice latex-math-preview-raise-can-not-create-image to not be executed if
;; the impatient mode is active. Otherwise, we would lost focus to the window
;; with the error, which is not desirable.
(defadvice latex-math-preview-raise-can-not-create-image (around test activate)
  (if (not latex-math-preview-impatient-mode)
      ad-do-it
	(message "LaTeX error. Check syntax."))
  )

(provide 'latex-math-preview-impatient)
;;; latex-math-preview-impatient.el ends here
