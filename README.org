#+TITLE: latex-math-preview-impatient
=latex-math-preview-impatient= allows the instant preview of LaTeX snippets
using =latex-math-preview=.

I created it to preview large/complex equations or when doing a lengthy derivation using the =IEEEeqnarray= environment.

* Custom packages and commands
The use of =latex-math-preview= allows the use of additional packages and custom commands. Hence, to add custom commands or always use some packages, customise the variable =latex-math-preview-latex-template-header=. Remember that =latex-math-preview= will search for =\usepackage{}= in the current TeX file.

See [[https://gitlab.com/latex-math-preview/latex-math-preview/-/blob/master/latex-math-preview.el][the documentation of latex-math-preview for more information]].

* Licence
  This software is licensed under the GNU Public Licence version 3 (GPL v3).
  It is a derivative work/incorporates code from [[https://gitlab.com/latex-math-preview/latex-math-preview/][latex-math-preview]] and
  [[https://github.com/yangsheng6810/org-latex-impatient][org-latex-impatitent]], both also licensed under the GPL v3.
